package com.uxs.prueba2.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.prueba2.services.AccountabilityService;

@RestController
@RequestMapping(value = "/restaurant/accountability")
public class AccountabilityRest {
	
	@Autowired
	AccountabilityService accountabilityService;
	
	@GetMapping("revenue")
	public ResponseEntity<Double> getTotalRevenue() {
		return ResponseEntity.ok(accountabilityService.getTotalRevenue());
	}
	
	@GetMapping("taxes")
	public ResponseEntity<Double> getTotalTaxes() {
			return ResponseEntity.ok(accountabilityService.getTotalTaxes());
	}
	
	@GetMapping("/offers/menus")
	public ResponseEntity<Integer> getTotalMenusServed() {
			return ResponseEntity.ok(accountabilityService.getTotalMenusServed());
	}
	
	@GetMapping("/offers/ratio")
	public ResponseEntity<Double> offersRatio() {
			return ResponseEntity.ok(accountabilityService.offersRatio());
	}
	
}
