package com.uxs.prueba2.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.prueba2.dto.MealDto;
import com.uxs.prueba2.services.MealService;

@RestController
@RequestMapping(value = "/restaurant/meals")
public class MealRest {
	
	@Autowired
	MealService mealService;
	
	@GetMapping()
	public ResponseEntity<List<MealDto>> getAll() {
		return ResponseEntity.ok(mealService.findAll());
	}
	
	@GetMapping("type")
	public ResponseEntity<List<MealDto>> getAllByCategory(@RequestParam("mealType") String mealType) {
		List<MealDto> response = new ArrayList<>();
		try {
			response = mealService.findAllByMealType(mealType);
			return ResponseEntity.ok(response);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().body(response);
		}
	}
	
	@PostMapping
	public ResponseEntity<MealDto> createMeal(@RequestBody MealDto meal) {
		MealDto mealCreated = new MealDto();
		try {
			mealCreated = mealService.create(meal);
			return ResponseEntity.ok(mealCreated);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().body(mealCreated);
		}
	}
}
