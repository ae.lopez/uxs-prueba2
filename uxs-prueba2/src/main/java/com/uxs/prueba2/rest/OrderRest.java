package com.uxs.prueba2.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.prueba2.dto.BillDto;
import com.uxs.prueba2.dto.OrderDto;
import com.uxs.prueba2.services.OrderService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/restaurant/orders")
public class OrderRest {

	@Autowired
	OrderService orderService;
	
	@PostMapping
	public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto order) throws NotFoundException {
		OrderDto orderCreted = new OrderDto();
		try {
			orderCreted = orderService.create(order);
			return ResponseEntity.ok(orderCreted);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.badRequest().body(orderCreted);
		}
	}
	
	@GetMapping("{id}/bill")
	public ResponseEntity<BillDto> getOrderBill(@PathVariable("id") Long id) {
		BillDto bill = orderService.getOrderBill(id);
		if (bill != null) {
			return ResponseEntity.ok(bill);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("{id}/pay")
	public ResponseEntity<HttpStatus> payOrder(@PathVariable("id") Long id) {
		return orderService.payOrder(id) != null ? ResponseEntity.ok(HttpStatus.ACCEPTED) : ResponseEntity.ok(HttpStatus.NOT_FOUND) ;
	}
}
