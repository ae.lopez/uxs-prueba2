package com.uxs.prueba2.enums;

public enum MealDiscountType {
	HAPPY_HOUR, NONE, DAILY_MENU, WEEKLY_MENU
}
