package com.uxs.prueba2.enums;

public enum MealType {
	STARTER, MAIN_COURSE, DESSERT, DRINK
}
