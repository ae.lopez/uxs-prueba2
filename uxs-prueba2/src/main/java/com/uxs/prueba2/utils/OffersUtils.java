package com.uxs.prueba2.utils;

import java.util.Calendar;
import java.util.Date;

public final class OffersUtils {
	
	private OffersUtils() {
		throw new IllegalStateException("This is an utility class and cannot be instantiated");
	}
	
	public static boolean isWeekend() {		
		Date now = new Date();
		Calendar c = Calendar.getInstance();
	    c.setTime(now);		
		return (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY);
	}
	
	public static boolean isHappyHour() {		
		int from = 1800;
	    int to = 2200;
	    Date date = new Date();
	    Calendar c = Calendar.getInstance();
	    c.setTime(date);
	    int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);
	    return to > from && t >= from && t <= to || to < from && (t >= from || t <= to);
	}
}
