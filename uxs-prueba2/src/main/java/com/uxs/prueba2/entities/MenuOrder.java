package com.uxs.prueba2.entities;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity(name = "MENU_ORDER")
public class MenuOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@NotNull
	private Long drinkId;
	
	@NotNull
	private Long mainCourseId;
	
	@NotNull
	private Long dessertId;
	
	@Column
	@NotNull
	private String menuType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(Long drinkId) {
		this.drinkId = drinkId;
	}

	public Long getMainCourseId() {
		return mainCourseId;
	}

	public void setMainCourseId(Long mainCourseId) {
		this.mainCourseId = mainCourseId;
	}

	public Long getDessertId() {
		return dessertId;
	}

	public void setDessertId(Long dessertId) {
		this.dessertId = dessertId;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}
}
