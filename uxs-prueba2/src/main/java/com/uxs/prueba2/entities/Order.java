package com.uxs.prueba2.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "ORDERS")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;

	@Column
	@Min(0)
	private double totalPrice;

	@Column
	@Min(0)
	private double totalTaxes;

	@Column
	private boolean payed;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<MenuOrder> menusOrdered;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<MealOrder> mealsOrdered;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getTotalTaxes() {
		return totalTaxes;
	}

	public void setTotalTaxes(double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}

	public boolean isPayed() {
		return payed;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}
	
	public List<MenuOrder> getMenusOrdered() {
		return menusOrdered;
	}

	public void setMenusOrdered(List<MenuOrder> menusOrdered) {
		this.menusOrdered = menusOrdered;
	}

	public List<MealOrder> getMealsOrdered() {
		return mealsOrdered;
	}

	public void setMealsOrdered(List<MealOrder> mealsOrdered) {
		this.mealsOrdered = mealsOrdered;
	}
}

