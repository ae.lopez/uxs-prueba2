package com.uxs.prueba2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.uxs.prueba2.configuration.SwaggerConfig;

@SpringBootApplication
@EnableScheduling
@Import(SwaggerConfig.class)
public class RestaurantApp implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantApp.class, args);
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	         registry.addResourceHandler("swagger-ui.html")
	      .addResourceLocations("classpath:/META-INF/resources/");
	}
}
