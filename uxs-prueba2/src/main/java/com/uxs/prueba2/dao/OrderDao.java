package com.uxs.prueba2.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.uxs.prueba2.entities.Order;

@Repository
public interface OrderDao extends JpaRepository<Order, Long> {
	
	

}
