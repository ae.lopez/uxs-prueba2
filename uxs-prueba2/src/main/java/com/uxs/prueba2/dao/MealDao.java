package com.uxs.prueba2.dao;

import org.springframework.stereotype.Repository;

import com.uxs.prueba2.entities.Meal;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface MealDao extends JpaRepository<Meal, Long> {
	
	List<Meal> findAllByMealType(String mealType);
}
