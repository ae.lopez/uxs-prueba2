package com.uxs.prueba2.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
public class OrikaMapperFactory {

	private MapperFactory mapperFactory;

	public OrikaMapperFactory() {
		if (mapperFactory == null) {
			mapperFactory = new DefaultMapperFactory.Builder().build();
		}
	}

	@Bean
	public MapperFacade getMapperFacade() {
		return this.mapperFactory.getMapperFacade();
	}

}
