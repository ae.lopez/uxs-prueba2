package com.uxs.prueba2.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.uxs.prueba2.dao.OrderDao;
import com.uxs.prueba2.dto.BillDto;
import com.uxs.prueba2.dto.OrderDto;
import com.uxs.prueba2.entities.MealOrder;
import com.uxs.prueba2.entities.MenuOrder;
import com.uxs.prueba2.entities.Order;
import com.uxs.prueba2.enums.MealDiscountType;

import javassist.NotFoundException;
import ma.glasnost.orika.MapperFacade;


@Service
@Transactional
public class OrderService {
	
	@Autowired
	OrderDao orderDao;
	@Autowired
	MapperFacade orika;
	@Autowired
	MealService mealService;
	@Autowired
	MenuOrderService menuOrderService;

	@Value("${bill.tax}")
	private double billTax;
	
	private static Logger logger = LogManager.getLogger(OrderService.class);

	public OrderDto create(OrderDto orderDto) throws NotFoundException {
		
		Order order = orika.map(orderDto, Order.class);

		List<MenuOrder> menusOrdered = order.getMenusOrdered();
		List<MealOrder> mealsOrdered = order.getMealsOrdered();
		double menusPrice = 0;
		double mealsPrice = 0;
		
		if (menusOrdered != null && !menusOrdered.isEmpty()) {
			order.setMenusOrdered(menuOrderService.setMenusOffers(menusOrdered));
			menuOrderService.consumeMenus(menusOrdered);
			menusPrice = menuOrderService.getMenusBill(menusOrdered);
		}
		
		if (mealsOrdered!= null && !mealsOrdered.isEmpty()) {
			order.setMealsOrdered(mealService.setMealsOffers(mealsOrdered));
			mealService.consumeMeals(mealsOrdered);
			mealsPrice = mealService.getMealsBill(mealsOrdered);
		}
		
		double price = menusPrice + mealsPrice;
		
		Double taxes = price * billTax;
		price += taxes;
		
		order.setTotalPrice(price);
		order.setTotalTaxes(taxes);		
		order.setPayed(false);
		
		Order savedOrder = orderDao.save(order);
		logger.info("Order " + savedOrder.getId() + " has been created");
			
		return orika.map(savedOrder, OrderDto.class);
	}

	public OrderDto findOrderById(Long id) {
		Optional<Order> order = orderDao.findById(id);
		return order.isPresent() ? orika.map(order.get(), OrderDto.class)  : null;
	}

	public Order payOrder(Long id) {
		Optional<Order> order = orderDao.findById(id);
		if (order.isPresent()) {
			order.get().setPayed(true);
			Order savedOrder = orderDao.save(order.get());
			logger.info("Order " + savedOrder.getId() + " has been payed");
			return savedOrder;
		} else {
			return null;
		}
	}

	public BillDto getOrderBill(Long id) {
		Optional<Order> order = orderDao.findById(id);
		if (order.isPresent()) {
			BillDto bill = orika.map(order.get(), BillDto.class);
			bill.setTax(billTax);
			return bill;
		} else {
			return null;
		}
	}

	public Double getTotalRevenue() {
		List<Order> orders = orderDao.findAll();
		return ordersPayed(orders).mapToDouble(order -> order.getTotalPrice()).sum();
	}
	
	public Double getTotalTaxes() {
		List<Order> orders = orderDao.findAll();
		return ordersPayed(orders).mapToDouble(order -> order.getTotalTaxes()).sum();
	}

	public Integer getTotalMenusServed() {
		List<Order> orders = orderDao.findAll();
		return ordersPayed(orders).mapToInt(order -> order.getMenusOrdered().size()).sum();
	}

	public Double offersRatio() {
		List<Order> orders = orderDao.findAll();
		List<MealOrder> mealsPayed = ordersPayed(orders).flatMap(order -> order.getMealsOrdered().stream()).collect(Collectors.toList());
		if (!mealsPayed.isEmpty()) {
			List<MealOrder> mealsPayedWithOffer = mealsPayed.stream().filter(meal -> !MealDiscountType.NONE.toString().equals(meal.getDiscountType()))
															.collect(Collectors.toList());
			Double totalMealsPayed = Double.valueOf(mealsPayed.size());
			Double totalMealsPayedWithOffer = Double.valueOf(mealsPayedWithOffer.size());
			
			return totalMealsPayedWithOffer/totalMealsPayed;
		} else {
			return 0.0;
		}
	}
	
	private Stream<Order> ordersPayed(List<Order> orders) {
		return orders.stream().filter(order -> order.isPayed());
	}
	
}
