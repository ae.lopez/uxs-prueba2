package com.uxs.prueba2.services;

import java.util.List;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Enums;
import com.uxs.prueba2.dao.MealDao;
import com.uxs.prueba2.dto.MealDto;
import com.uxs.prueba2.entities.Meal;
import com.uxs.prueba2.entities.MealOrder;
import com.uxs.prueba2.enums.MealDiscountType;
import com.uxs.prueba2.enums.MealType;
import com.uxs.prueba2.utils.OffersUtils;

import javassist.NotFoundException;
import ma.glasnost.orika.MapperFacade;

@Service
@Transactional
public class MealService {

	@Autowired
	MealDao mealDao;
	@Autowired
	MapperFacade orika;
	
	@Value("${offers.drinks.happyhour.discount}")
	private double happyHourDiscount;
	@Value("${meal.refill}")
	private int refill;

	private static Logger logger = LogManager.getLogger(MealService.class);
	
	public List<MealDto> findAll() {
		return orika.mapAsList(mealDao.findAll(), MealDto.class);
	}
	
	public List<MealDto> findAllByMealType(String mealType) {
		if (Enums.getIfPresent(MealType.class, mealType).isPresent()) {
			return orika.mapAsList(mealDao.findAllByMealType(mealType), MealDto.class);
		} else {
			throw new IllegalArgumentException();
		}	
	}

	public MealDto create(MealDto mealDto) {
		if (Enums.getIfPresent(MealType.class, mealDto.getMealType()).isPresent()) {
			Meal meal = orika.map(mealDto, Meal.class);
			Meal savedMeal = mealDao.save(meal);
			logger.info("Meal " + savedMeal.getId() + "has been created");
			return orika.map(savedMeal, MealDto.class);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	public void consumeMeals(List<MealOrder> mealsOrdered) throws NotFoundException {
		for (MealOrder mealOrder : mealsOrdered) {
			Optional<Meal> meal = mealDao.findById(mealOrder.getMealId());
			if (meal.isPresent()) {
				consumeMeal(meal.get(), mealOrder.getQuantity());
			} else {
				throw new NotFoundException("Meal with id: " + mealOrder.getMealId() + " was not found");
			}
		}
	}
	
	Meal consumeMeal(Meal meal, int quantity) {
		int mealStock = meal.getStock();
		if (mealStock < quantity) {
			String error = "The quantity ordered of " + meal.getName() + " is greater than the stock";
			logger.error(error);
			throw new IllegalArgumentException(error);
		} else {
			meal.setStock(mealStock - quantity);
			if (meal.getStock() == 0) {
				refillStock(meal);
			}
			return mealDao.save(meal);
		}
	}
	
	private Meal refillStock(Meal meal) {
		meal.setStock(refill);
		logger.info("Meal " + meal.getId() + " has been refilled");
		return meal;
	}

	public List<MealOrder> setMealsOffers(List<MealOrder> mealsOrdered) throws NotFoundException {
		for (MealOrder mealOrder : mealsOrdered) {
			Optional<Meal> meal = mealDao.findById(mealOrder.getMealId());
			if (meal.isPresent()) {
				Double mealPrice = meal.get().getPrice();
				int quantity = mealOrder.getQuantity();
				if (OffersUtils.isHappyHour()) {
					mealOrder.setDiscountType(MealDiscountType.HAPPY_HOUR.toString()); 
					mealOrder.setDiscount(mealPrice* quantity * happyHourDiscount);
				} else {
					mealOrder.setDiscountType(MealDiscountType.NONE.toString()); 
					mealOrder.setDiscount(0);
				}
			} else {
				throw new NotFoundException("Meal with id: " + mealOrder.getMealId() + " was not found");
			}
		}
		return mealsOrdered;
	}
	
	public double getMealsBill(List<MealOrder> mealsOrdered) throws NotFoundException {
		double price = 0;
		for (MealOrder mealOrder : mealsOrdered) {
			Optional<Meal> meal = mealDao.findById(mealOrder.getMealId());
			if (meal.isPresent()) {
				Double mealPrice = meal.get().getPrice();
				int quantity = mealOrder.getQuantity();
				price += ((mealPrice * quantity) - mealOrder.getDiscount());
			} else {
				throw new NotFoundException("Meal with id: " + mealOrder.getMealId() + " was not found");
			}
		}
		return price;
	}
}
