package com.uxs.prueba2.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.uxs.prueba2.entities.MealOrder;
import com.uxs.prueba2.entities.MenuOrder;
import com.uxs.prueba2.enums.MealDiscountType;
import com.uxs.prueba2.utils.OffersUtils;

import javassist.NotFoundException;

@Service
public class MenuOrderService {
	
	@Autowired
	MealService mealService;
	
	@Value("${offers.menu.daily.price}")
	private int dailyMenu;
	@Value("${offers.menu.weekend.price}")
	private int weeklyMenu;
		
	public List<MenuOrder> setMenusOffers(List<MenuOrder> menusOrdered) {
		for (MenuOrder menu : menusOrdered) {
			menu.setMenuType(OffersUtils.isWeekend() ? MealDiscountType.WEEKLY_MENU.toString() : MealDiscountType.DAILY_MENU.toString());
		}
		return menusOrdered;
	}
	
	public void consumeMenus(List<MenuOrder> menusOrdered) throws NotFoundException {
		List<MealOrder> menuMeals = new ArrayList<>();
		for (MenuOrder menu : menusOrdered) {
			MealOrder drink = new MealOrder();
			drink.setMealId(menu.getDrinkId());
			drink.setQuantity(1);
			MealOrder mainCourse = new MealOrder();
			mainCourse.setMealId(menu.getMainCourseId());
			mainCourse.setQuantity(1);
			MealOrder dessert = new MealOrder();
			dessert.setMealId(menu.getDessertId());
			dessert.setQuantity(1);
			menuMeals.add(drink);
			menuMeals.add(mainCourse);
			menuMeals.add(dessert);
		}
		mealService.consumeMeals(menuMeals);
	}
	
	public double getMenusBill(List<MenuOrder> menusOrdered) {
		int numberOfMenus = menusOrdered.size();
		return OffersUtils.isWeekend() ? weeklyMenu * numberOfMenus  : dailyMenu * numberOfMenus;
	}
}
