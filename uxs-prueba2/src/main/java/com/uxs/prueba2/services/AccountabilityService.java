package com.uxs.prueba2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountabilityService {

	@Autowired
	OrderService orderService;

	public Double getTotalRevenue() {
		return orderService.getTotalRevenue();
	}

	public Double getTotalTaxes() {
		return orderService.getTotalTaxes();
	}

	public Integer getTotalMenusServed() {
		return orderService.getTotalMenusServed();
	}

	public Double offersRatio() {
		return orderService.offersRatio();
	}
}
