package com.uxs.prueba2.dto;

import java.util.List;

public class OrderDto {
	
	private List<MenuOrderDto> menusOrdered;
	private List<MealOrderDto> mealsOrdered;
	
	public List<MenuOrderDto> getMenusOrdered() {
		return menusOrdered;
	}
	public void setMenusOrdered(List<MenuOrderDto> menusOrdered) {
		this.menusOrdered = menusOrdered;
	}
	public List<MealOrderDto> getMealsOrdered() {
		return mealsOrdered;
	}
	public void setMealsOrdered(List<MealOrderDto> mealsOrdered) {
		this.mealsOrdered = mealsOrdered;
	}
}
