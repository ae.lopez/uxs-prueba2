package com.uxs.prueba2.dto;

public class MealOrderDto {
	
	private Long mealId;
	private int quantity;
	
	public Long getMealId() {
		return mealId;
	}
	public void setMealId(Long mealId) {
		this.mealId = mealId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
