package com.uxs.prueba2.dto;

import java.util.List;

public class BillDto {
	
	private List<MenuOrderDto> menusOrdered;
	private List<MealOrderDto> mealsOrdered;
	private double totalPrice;
	private double tax;
	private double totalTaxes;
	
	
	public List<MenuOrderDto> getMenusOrdered() {
		return menusOrdered;
	}
	public void setMenusOrdered(List<MenuOrderDto> menusOrdered) {
		this.menusOrdered = menusOrdered;
	}
	public List<MealOrderDto> getMealsOrdered() {
		return mealsOrdered;
	}
	public void setMealsOrdered(List<MealOrderDto> mealsOrdered) {
		this.mealsOrdered = mealsOrdered;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getTotalTaxes() {
		return totalTaxes;
	}
	public void setTotalTaxes(double totalTaxes) {
		this.totalTaxes = totalTaxes;
	}
}
