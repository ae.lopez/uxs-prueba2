package com.uxs.prueba2.dto;

public class MenuOrderDto {
	
	private Long drinkId;
	private Long mainCourseId;
	private Long dessertId;
	
	public Long getDrinkId() {
		return drinkId;
	}
	public void setDrinkId(Long drinkId) {
		this.drinkId = drinkId;
	}
	public Long getMainCourseId() {
		return mainCourseId;
	}
	public void setMainCourseId(Long mainCourseId) {
		this.mainCourseId = mainCourseId;
	}
	public Long getDessertId() {
		return dessertId;
	}
	public void setDessertId(Long dessertId) {
		this.dessertId = dessertId;
	}
}
