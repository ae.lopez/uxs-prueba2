package com.uxs.prueba2.services;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.uxs.prueba2.dao.OrderDao;
import com.uxs.prueba2.entities.Order;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
	
	@InjectMocks
	OrderService orderService;
	@Mock
	OrderDao orderDao;
	
	@Test
	public void getTotalRevenue_nothingPayed() {
		Mockito.when(orderDao.findAll()).thenReturn(new ArrayList<Order>());
		Double revenue = 0.0;
		assertTrue(revenue.equals(orderService.getTotalRevenue()));
	}
	
	@Test
	public void getTotalRevenue_ordersPayed() {
		List<Order> orders = new ArrayList<>();
		
		Order order1 = new Order();
		order1.setPayed(true);
		order1.setTotalPrice(30.40);
		orders.add(order1);		
		Order order2 = new Order();
		order2.setPayed(false);
		order2.setTotalPrice(2.80);
		orders.add(order2);
		
		Mockito.when(orderDao.findAll()).thenReturn(orders);
		Double revenue = 30.40;
		assertTrue(revenue.equals(orderService.getTotalRevenue()));
	}
	
}
