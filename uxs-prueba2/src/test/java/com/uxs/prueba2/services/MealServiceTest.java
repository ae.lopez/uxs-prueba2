package com.uxs.prueba2.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Enums;
import com.uxs.prueba2.configuration.OrikaMapperFactory;
import com.uxs.prueba2.dao.MealDao;
import com.uxs.prueba2.dto.MealDto;
import com.uxs.prueba2.entities.Meal;
import com.uxs.prueba2.entities.MealOrder;
import com.uxs.prueba2.enums.MealDiscountType;
import com.uxs.prueba2.enums.MealType;

import javassist.NotFoundException;
import ma.glasnost.orika.MapperFacade;

@RunWith(MockitoJUnitRunner.class)
public class MealServiceTest {
	
	@InjectMocks
	MealService mealService;
	@Mock
	MealDao mealDao;
	@Mock
	MapperFacade orika;
	
	@Test(expected = IllegalArgumentException.class)
	public void consumeMeal_greaterQuantityThanStock_Exception() {
		Meal meal = newMeal();
		int quantity = 3;
		mealService.consumeMeal(meal,quantity);
	}
	
	@Test
	public void consumeMeal_greaterQuantityThanStock_ExceptionMessage() {
		Meal meal = newMeal();
		int quantity = 3;
		try {
			mealService.consumeMeal(meal,quantity);
		} catch (IllegalArgumentException e) {
			assertEquals("The quantity ordered of Brownie is greater than the stock", e.getMessage());
		}
	}
	
	@Test
	public void consumeMeal_sameQuantityAsStock_MealRefilled() {
		Meal meal = newMeal();
		int quantity = 2;
		Meal mealRefilled = newMeal();
		mealRefilled.setStock(10);
		Mockito.when(mealDao.save(meal)).thenReturn(mealRefilled);
		Meal response = mealService.consumeMeal(meal,quantity);
		assertEquals(10, response.getStock());
	}
	
	@Test
	public void consumeMeal_lessQuantityThanStock_MealRefilled() {
		Meal meal = newMeal();
		int quantity = 1;
		Meal mealReturned = newMeal();;
		mealReturned.setStock(1);
		Mockito.when(mealDao.save(meal)).thenReturn(mealReturned);
		Meal response = mealService.consumeMeal(meal,quantity);
		assertEquals(1, response.getStock());
	}
	
	@Test
	public void create_meal_ok() {
		Meal mealSaved = new Meal();
		Meal meal = newMeal();
		meal.setId(null);
		
		MealDto mealDto = new MealDto();
		mealDto.setStock(2);
		mealDto.setName("Brownie");
		mealDto.setMealType(MealType.DESSERT.toString());
		mealDto.setPrice(2.50);
		
		MealDto mealDtoSaved = mealDto;
		mealDtoSaved.setId(1L);
		
		Mockito.when(orika.map(mealDto, Meal.class)).thenReturn(meal);
		Mockito.when(mealDao.save(meal)).thenReturn(mealSaved);
		Mockito.when(orika.map(mealSaved, MealDto.class)).thenReturn(mealDtoSaved);
		assertEquals(mealDtoSaved, mealService.create(mealDto));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void create_mealWrongType_Exception() {
		MealDto mealDto = new MealDto();
		mealDto.setMealType("BLE");
		mealService.create(mealDto);
	}

	/*
	@Test
	public void getMealsBill_withoutDiscount() throws NotFoundException {
		MealOrder mealOrder = new MealOrder();
		mealOrder.setDiscount(0);
		mealOrder.setDiscountType(MealDiscountType.NONE.toString());
		mealOrder.setId(1L);
		mealOrder.setMealId(1L);
		mealOrder.setQuantity(3);
		
		List<MealOrder> mealsOrdered = new ArrayList<>();
		mealsOrdered.add(mealOrder);
		
		Meal meal = newMeal();
		double expected = 7.50;
		double result = mealService.getMealsBill(mealsOrdered);
		Mockito.when(mealDao.findById(1L)).thenReturn(Optional.of(meal));
		assertTrue(expected == result);
	}*/

	private Meal newMeal() {
		Meal meal = new Meal();
		meal.setId(1L);
		meal.setStock(2);
		meal.setName("Brownie");
		meal.setMealType(MealType.DESSERT.toString());
		meal.setPrice(2.50);
		return meal;
	}

	
}
