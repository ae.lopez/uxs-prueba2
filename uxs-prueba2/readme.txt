The solution that i propose is an architecture based on SpringBoot, Hibernate and H2.
I've also integrated Swagger2 to make more accesible the API and could document it.

The route configured so you could test it it's: http://localhost:8081/swagger-ui.html#!
The route of the h2 to see the data base as you interact with the api: http://localhost:8081/h2

Enjoy :)